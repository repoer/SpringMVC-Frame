package com.admin.user.dao;

import com.admin.user.entity.UserEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created by tianke on 16/8/23.
 */
public interface UserDao {
    List<UserEntity> list();
    UserEntity getUser(@Param("id") int id);
}
