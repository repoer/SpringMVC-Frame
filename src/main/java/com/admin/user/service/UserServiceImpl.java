package com.admin.user.service;

import com.admin.user.dao.UserDao;
import com.admin.user.entity.UserEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by tianke on 16/8/23.
 */
@Service
public class UserServiceImpl implements IUserService {

    @Autowired
    private UserDao userDao;

    @Override
    public List<UserEntity> list() {
        return userDao.list();
    }

    @Override
    public UserEntity getUser(int id) {
        return userDao.getUser(id);
    }
}
