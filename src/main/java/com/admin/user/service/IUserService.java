package com.admin.user.service;

import com.admin.user.entity.UserEntity;

import java.util.List;

/**
 * Created by tianke on 16/8/23.
 */
public interface IUserService {

    /**
     * 获取User列表
     * @return
     */
    List<UserEntity> list();

    /**
     * 获取指定id的User
     * @param id
     * @return
     */
    UserEntity getUser(int id);

}
