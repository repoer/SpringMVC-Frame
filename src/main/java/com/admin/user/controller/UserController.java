package com.admin.user.controller;

import com.admin.common.controller.AppController;
import com.admin.common.param.RtModel;
import com.admin.user.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by tianke on 16/8/23.
 */
@Controller
@RequestMapping(value = "/admin/user")
public class UserController extends AppController {

    @Autowired
    private IUserService userService;

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    @ResponseBody
    public RtModel userList() {
        RtModel rm = new RtModel();

        try {
            rm.setCode(1);
            rm.setData(userService.list());
        } catch (Exception e) {
            e.printStackTrace();
            rm.setCode(0);
        }

        return rm;
    }

    @RequestMapping("/getUser")
    @ResponseBody
    public RtModel getUser(@RequestParam("id") int id) {
        RtModel rm = new RtModel();

        try {
            rm.setCode(1);
            rm.setData(userService.getUser(id));
        } catch (Exception e) {
            e.printStackTrace();
            rm.setCode(0);
        }

        return rm;
    }

}
