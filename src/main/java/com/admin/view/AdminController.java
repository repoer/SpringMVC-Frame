package com.admin.view;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
/**
 * Created by tianke on 16/8/23.
 */
@Controller
@RequestMapping("/admin")
public class AdminController {

    @RequestMapping("/")
    public String admin() {
        return "/index";
    }

    @RequestMapping("/index")
    public String index() {
        return "/index";
    }

}
